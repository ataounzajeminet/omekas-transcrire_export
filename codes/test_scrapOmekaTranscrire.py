# -*- coding: utf-8 -*-
"""
Created on Mon May 17 09:32:51 2021

@author: Michael Nauge, Poitiers Univeristy
"""

import sys

import scrapOmekaTranscrire as sot

from pathlib import Path

import pandas as pd



def omkTransPage2StrJpg_test():
    urlTarget = "https://transcrire.huma-num.fr/scripto/8/105/10286"
    
    jpgId = sot.omkTransPage2StrJpg(urlTarget)
    print(jpgId)
    
    

def omkTransPage2StrTrans_test():
    
    #urlTarget = "https://transcrire.huma-num.fr/scripto/1/20/554"
    urlTarget = "https://transcrire.huma-num.fr/scripto/13/17001/17399"
    
    print("try to get "+urlTarget)

    try :
        strTrans = sot.omkTransPage2StrTrans(urlTarget, False)
        
        print(strTrans)
        
        strTransMarkuped = sot.omkTransPage2StrTrans(urlTarget, True)
        print(strTransMarkuped)
        
        
    except ValueError:
        print("error omkTransPage2StrTrans")

def omkTransBS2StrTrans_test():
    
    #urlTarget = "https://transcrire.huma-num.fr/scripto/15/18600/media"
    urlTarget = "https://transcrire.huma-num.fr/scripto/13/17001/17399"
    
    print("try to get "+urlTarget)

    try :
        pageSoup = sot.urlInterrogation(urlTarget)
        
        strTrans = sot.omkTransBS2StrTrans(pageSoup, False)
        
        print(strTrans)
        
        strTransMarkuped = sot.omkTransBS2StrTrans(pageSoup, True)
        print(strTransMarkuped)
        
        
    except ValueError:
        print("error omkTransBS2StrTrans_test")



def omkTransPage2Txt_test():
    urlTarget = "https://transcrire.huma-num.fr/scripto/8/105/10286"
    pathDirTxt = "./../datas/01-primary/"
    
    sot.omkTransPage2Txt(urlTarget, pathDirTxt)


    
    
    
    
def omkTransCarnet2listUrlPages_test():
    urlTarget = "https://transcrire.huma-num.fr/scripto/10/76/media"
    
    listUrl = sot.omkTransCarnet2listUrlPages(urlTarget)
    print(listUrl)


def omkTransCarnet2Txt_test():
    
    print("start extraction")
    
    urlTargetCarnet="https://transcrire.huma-num.fr/scripto/8/127/media"
    pathDirTxt="./../datas/Correspondance avec Maurice Laporte-Bisquit"
    

    Path(pathDirTxt).mkdir(parents=True, exist_ok=True)
    
    sot.omkTransCarnet2Txt(urlTargetCarnet, pathDirTxt)
    
    
def getDictAuthorTranscript_test():
    
    idPage = "8/122/11398"
    lisdicHist = sot.getDictAuthorTranscript(idPage)
    
    for dic in lisdicHist:
        print(dic)
    
def omkTransCarnet2dfHistContrib_test():
    
    urlTargetCarnet = "https://transcrire.huma-num.fr/scripto/8/122/media"
    
    dfRes = sot.omkTransCarnet2dfHistContrib(urlTargetCarnet)
    
    print(dfRes)


def dirTxt2df_test():
    pathDirTxt = "./../datas/FRAD86_16J3/"
    dfTrans = sot.dirTxt2df(pathDirTxt)
    #print(dfTrans)
    
    dfTrans.to_excel("../datas/FRAD86_16J3/transMerged.xlsx")
    
    
    
def omkTransCarnetGetTitle_test():
    urlTargetCarnet = "https://transcrire.huma-num.fr/scripto/8/127/media"
    carnetName = sot.omkTransCarnetGetTitle(urlTargetCarnet)
    print(carnetName)
    
    #remplacer les caractères qui ne vont pas pour un nom de dossier.
    validName = ""
    for c in carnetName:
        if c.isalnum():
            validName+=c
        else:
            validName+="_"
    print(validName)
    
    
def extractCarnet_test():
    #urlTargetCarnet = "https://transcrire.huma-num.fr/scripto/8/122/media"
    urlTargetCarnet = "https://transcrire.huma-num.fr/scripto/8/108/media"
    sot.extractCarnet(urlTargetCarnet)
    
    
def main(argv):
    
    #omkTransCarnetGetTitle_test()
    
    #omkTransPage2Txt_test()
    
    #omkTransPage2StrJpg_test()
    
    #omkTransPage2StrTrans_test()
    
    #omkTransCarnet2listUrlPages_test()
    
    #omkTransCarnet2Txt_test()
    
    #getDictAuthorTranscript_test()
    
    #omkTransCarnet2dfHistContrib_test()
    
    #dirTxt2df_test()
    
    
    #omkTransPage2StrTrans_test()
    omkTransBS2StrTrans_test()
    
if __name__ == "__main__":
    main(sys.argv) 